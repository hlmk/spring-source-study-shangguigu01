package com.cht.test;

import com.cht.aop.MathCalculator;
import com.cht.config.MainConfigOfAop;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTest_Aop {

    @Test
    public void test01(){
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfAop.class);
        MathCalculator mathCalculator = applicationContext.getBean(MathCalculator.class);
        int div = mathCalculator.div(6, 3);
        System.out.println("运行结果为：" + div);
    }

}
