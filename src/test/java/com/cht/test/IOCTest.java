package com.cht.test;

import com.cht.bean.Person;
import com.cht.bean.RainBow;
import com.cht.config.MainConfig;
import com.cht.config.MainConfig2;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sun.applet.Main;

import java.util.Map;

public class IOCTest {

    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig2.class);

    @Test
    public void test4(){
        printName();
        Object colorBeanFactory = applicationContext.getBean("colorBeanFactory");
        Object colorBeanFactory1 = applicationContext.getBean("colorBeanFactory");
        System.out.println(colorBeanFactory == colorBeanFactory1);
        System.out.println(colorBeanFactory.getClass());
    }

    public void printName(){
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String name: beanDefinitionNames) {
            System.out.println(name);
        }
    }


    @Test
    public void test3(){
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig2.class);
        String[] namesForType = applicationContext.getBeanNamesForType(Person.class);
        for (String name: namesForType) {
            System.out.println(name);
        }
        Map<String, Person> beansOfType = applicationContext.getBeansOfType(Person.class);
        System.out.println(beansOfType);
    }

    @Test
    public void test2(){
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig2.class);
//        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
//        for (String name: beanDefinitionNames             ) {
//            System.out.println(name);
//        }
        System.out.println("IOC容器初始化完成===");
        Person bean = applicationContext.getBean(Person.class);
        Person bean2 = applicationContext.getBean(Person.class);
        System.out.println(bean == bean2);
    }


    @Test
    public void test(){
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig.class);
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String name: beanDefinitionNames             ) {
            System.out.println(name);
        }
    }

}
