package com.cht.test;

import com.cht.bean.Car;
import com.cht.config.MainConfigOfLifeCycle;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTest_LifeCycle {

    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfLifeCycle.class);

    @Test
    public void test(){
        System.out.println("IOC 容器初始化完成......");
        Car bean = applicationContext.getBean(Car.class);
        Car bean2 = applicationContext.getBean(Car.class);
//        bean.destroy();
        applicationContext.close();
    }

}
