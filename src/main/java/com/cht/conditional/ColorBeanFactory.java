package com.cht.conditional;

import com.cht.bean.Color;
import org.springframework.beans.factory.FactoryBean;

public class ColorBeanFactory implements FactoryBean {

    @Override
    public Object getObject() throws Exception {
        return new Color();
    }

    @Override
    public Class<?> getObjectType() {
        return Color.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
