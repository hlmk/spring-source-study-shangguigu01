package com.cht.conditional;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class WindowsConditional implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
//        System.out.println("==="+beanFactory);
        ClassLoader classLoader = context.getClassLoader();
//        System.out.println("==="+classLoader);
        Environment environment = context.getEnvironment();
//        System.out.println("==="+environment);
        BeanDefinitionRegistry registry = context.getRegistry();
//        System.out.println("==="+registry);
        ResourceLoader resourceLoader = context.getResourceLoader();
//        System.out.println("==="+resourceLoader);
        System.out.println("os.name---->" + environment.getProperty("os.name"));
        if(environment.getProperty("os.name").contains("Windows")){
            return true;
        }
        return false;
    }
}
