package com.cht.conditional;

import com.cht.bean.RainBow;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(RainBow.class);
        rootBeanDefinition.setScope("singleton");
        boolean b1 = registry.containsBeanDefinition("com.cht.bean.Red");
        boolean b = registry.containsBeanDefinition("com.cht.bean.Blue");
        if(b && b1){
            registry.registerBeanDefinition("rainBow", rootBeanDefinition);
        }
    }
}
