package com.cht.config;

import com.cht.aop.LogAspects;
import com.cht.aop.MathCalculator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * AspectJAutoProxyRegistrar
 * AnnotationAwareAspectJAutoProxyCreator
 *      => AspectJAwareAdvisorAutoProxyCreator
 *          => AbstractAdvisorAutoProxyCreator
 *              => AbstractAutoProxyCreator  ==> SmartInstantiationAwareBeanPostProcessor BeanFactoryAware
 *                  => ProxyProcessorSupport
 *
 */

@EnableAspectJAutoProxy
@Configuration
public class MainConfigOfAop {

    @Bean
    public MathCalculator mathCalculator(){
        return new MathCalculator();
    }

    @Bean
    public LogAspects logAspects(){
        return new LogAspects();
    }


}
