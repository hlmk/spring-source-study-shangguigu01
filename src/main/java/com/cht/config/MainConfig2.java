package com.cht.config;

import com.cht.bean.Color;
import com.cht.bean.Person;
import com.cht.bean.Red;
import com.cht.conditional.*;
import org.springframework.context.annotation.*;

//@Conditional(WindowsConditional.class)
@Configuration
@Import({Red.class, MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})
public class MainConfig2 {

    @Lazy(value = true)
    @Scope(value = "singleton")
//    @Scope(value = "prototype")
    @Bean
    public Person person(){
        System.out.println("person对象创建=====》");
        return new Person("王五", 21);
    }

    @Conditional(value = WindowsConditional.class)
    @Bean
    public Person person01(){
        return new Person("bill gacs", 66);
    }

    @Conditional({LinuxConditional.class})
    @Bean
    public Person person02(){
        return new Person("linus", 48);
    }

    @Bean
    public ColorBeanFactory colorBeanFactory(){
        return new ColorBeanFactory();
    }

}
